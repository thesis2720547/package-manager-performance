# Benchmark

Benchmarking is done with [hyperfine](https://github.com/sharkdp/hyperfine).

The tests are run inside an Ubuntu Jammy 22.04 AMD64 virtual machine with 1 vCPU and 1GB in memory.

### package-manager
Compares installation speed of NPM, Yarn and PNPM.
- _package.json_: Copied from our server application repository, and contains the specifications on what dependencies to install (37 total).
- _script.sh_: Hyperfine script.
- _result.json_: Result of hyperfine script.
- _summary.txt_: Summary of hyperfine result.
- _figure.png_: Plot of hyperfine result.
### video-extraction
Measures the speed of extracting audio and video frames from target video.
- _test.mp4_: Target video with 14.24s in duration and 29.97 Frames Per Second (FPS).
- _script.sh_: Hyperfine script.
- _result.json_: Result of hyperfine script.
- _summary.txt_: Summary of hyperfine result.
- _figure.png_: Plot of hyperfine result.
